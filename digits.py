import sys

def convert(num):
    return (list(map(int,str(num))))
    

def lastcount(num1):
    list_num = convert(num1)
    count = 1
    for i in range((len(list_num)-1), -1, -1):
        if (list_num[i] != list_num[i - 1]):
            break
        else:
            count += 1
    return count

def trailing_digits(doodad_price,trailing,max_price):
    counter = []
    doodad_list = []
    k=1000
    for i in range(1, k):
        num = doodad_price * i
        if (num > max_price):
            break
        else:
            doodad_list.append(num)
    for i in doodad_list:
        last_digit = convert(i)[-1]
        if (last_digit == trailing):
            counts = lastcount(i)
            counter.append(counts)
    return max(counter)

print(trailing_digits(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3])))

